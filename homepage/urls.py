from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('myhooby/', views.myhooby, name='myhooby'),
    path('contact/', views.contact, name='contact'),
    path('resume/', views.resume, name='resume'),

    # dilanjutkan ...
]